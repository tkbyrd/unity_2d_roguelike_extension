﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MovingObject {

	public int wallDamage = 1;
	public int pointsPerFood = 10;
	public int pointsPerSoda = 20;
    public float restartLevelDelay = 1f;
    public Text foodText;
    public AudioClip moveSound1;
    public AudioClip moveSound2;
    public AudioClip eatSound1;
    public AudioClip eatSound2;
    public AudioClip drinkSound1;
    public AudioClip drinkSound2;
    public AudioClip gameOverSound;

    private Animator animator;
	private int food;
    private int trapTurnsLeft = 0;
    private int trapTurns = 5;
    private int repellantTurns = 10;
    private int repellantTurnsLeft = 0;
    public bool isRepelling = false;

	// Use this for initialization
	protected override void Start () {
		animator = GetComponent<Animator> ();

		food = GameManager1.instance.playerFoodPoints;

        foodText.text = "Food: " + food;

		base.Start ();
	}

	private void OnDisable()
	{
		GameManager1.instance.playerFoodPoints = food;
	}

	protected override void AttemptMove<T>(int xDir, int yDir)
	{
		food--;
        foodText.text = "Food: " + food;
        if (trapTurnsLeft > 0)
        {
            trapTurnsLeft -= 1;
        }
        else
        {
            base.AttemptMove<T>(xDir, yDir);
            RaycastHit2D hit;
            if (Move(xDir, yDir, out hit))
            {
                SoundManager.instance.RandomizeSfx(moveSound1, moveSound2);
            }
            if (repellantTurnsLeft > 0)
            {
                repellantTurnsLeft -= 1;
            }
            else
            {
                isRepelling = false;
            }
        }
        CheckIfGameOver();
        GameManager1.instance.playersTurn = false;
    }

	private void CheckIfGameOver()
	{
		if (food <= 0)
        {
            SoundManager.instance.PlaySingle(gameOverSound);
            SoundManager.instance.musicSource.Stop();
            GameManager1.instance.GameOver();
		}
	}

	private void OnTriggerEnter2D (Collider2D other)
	{

		if (other.tag == "Exit") {
			Invoke ("Restart", restartLevelDelay);
			enabled = false;
		} else if (other.tag == "Food") {
			food += pointsPerFood;
            foodText.text = "+" + pointsPerFood + " Food: " + food;
            SoundManager.instance.RandomizeSfx(eatSound1, eatSound2);
            other.gameObject.SetActive (false);
		} else if (other.tag == "Soda") {
			food += pointsPerSoda;
            foodText.text = "+" + pointsPerSoda + " Food: " + food;
            SoundManager.instance.RandomizeSfx(drinkSound1, drinkSound2);
            other.gameObject.SetActive (false);
		}
        else if (other.tag == "Trap")
        {
            trapTurnsLeft = trapTurns;
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "Repellent")
        {
            isRepelling = true;
            repellantTurnsLeft = repellantTurns;
            other.gameObject.SetActive(false);
        }
    }

	protected override void OnCantMove<T> (T component)
	{
		Wall hitWall = component as Wall;
		hitWall.DamageWall (wallDamage);
		animator.SetTrigger ("playerChop");
	}

	private void Restart()
	{
		SceneManager.LoadScene (0);
	}

	public void LoseFood(int loss)
	{
		animator.SetTrigger ("playerHit");
		food -= loss;
        foodText.text = "-" + loss + " Food: " + food;
        CheckIfGameOver ();
	}

	// Update is called once per frame
	void Update () {

		if (!GameManager1.instance.playersTurn)
			return;

		int horizontal = 0;
		int vertical = 0;

		horizontal = (int)Input.GetAxisRaw ("Horizontal");
		vertical = (int)Input.GetAxisRaw ("Vertical");

		if (horizontal != 0)
			vertical = 0;
		if (horizontal != 0 || vertical != 0) {
			AttemptMove<Wall> (horizontal, vertical);
		}

        GameManager1.instance.playerIsRepelling = isRepelling;

	}
}
