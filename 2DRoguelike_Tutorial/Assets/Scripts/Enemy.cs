﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MovingObject {

	public int playerDamage;

	private Animator animator;
	private Transform target;
	private bool skipMove;
    private int trapTurns = 3;
    private int trapTurnsLeft = 0;
    public AudioClip enemyAttack1;
    public AudioClip enemyAttack2;
    public bool isPatroller;
    private bool patrollerMoveRight = true;

    // Use this for initialization
    protected override void Start () {
		GameManager1.instance.AddEnemyToList (this);
		animator = GetComponent<Animator> ();
		target = GameObject.FindGameObjectWithTag ("Player").transform;
		base.Start ();
	}

	protected override void AttemptMove<T> (int xDir, int yDir)
	{
		if (skipMove && !isPatroller) {
			skipMove = false;
			return;
		}
        else if(trapTurnsLeft > 0)
        {
            trapTurnsLeft -= 1;
            return;
        }

		base.AttemptMove<T> (xDir, yDir);
		skipMove = true;
	}

	public void MoveEnemy()
	{
		int xDir = 0;
		int yDir = 0;

        if(isPatroller)
        {
            /*bool isHittingWall = false;
            GameObject[] wallTiles = GameManager1.instance.boardScript.wallTiles;
            for (int i = 0; i < wallTiles.Length; i++)
            {
                if (wallTiles[i].transform.position.x + 1 == this.transform.position.x || wallTiles[i].transform.position.x - 1 == this.transform.position.x)
                {
                    isHittingWall = true;
                }
            }*/
            if (this.transform.position.x <= 0)
            {
                patrollerMoveRight = true;
            }
            else if (this.transform.position.x >= 7)
            {
                patrollerMoveRight = false;
            }
            /*else if(isHittingWall)
            {
                patrollerMoveRight = !patrollerMoveRight;
            }*/

            if(patrollerMoveRight)
            {
                xDir = 1;
            }
            else
            {
                xDir = -1;
            }
        }
        else
        {
            if (Mathf.Abs(target.position.x - transform.position.x) < float.Epsilon)
            {
                yDir = target.position.y > transform.position.y ? 1 : -1;
            }
            else
            {
                xDir = target.position.x > transform.position.x ? 1 : -1;
            }
        }
		
        if (GameManager1.instance.playerIsRepelling && !isPatroller)
        {
            xDir *= -1;
            yDir *= -1;
        }


        AttemptMove<Player> (xDir, yDir);
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Trap")
        {
            trapTurnsLeft = trapTurns;
            other.gameObject.SetActive(false);
        }
    }

    protected override void OnCantMove <T> (T component)
	{
        Player hitPlayer = component as Player;
		hitPlayer.LoseFood(playerDamage);
        animator.SetTrigger("enemyAttack");
        SoundManager.instance.RandomizeSfx(enemyAttack1, enemyAttack2);
    }

	// Update is called once per frame
	//void Update () {
		
	//}
}
